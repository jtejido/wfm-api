<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use \App\Http\Controllers\Controller;
use \App\MUInterval;
use \App\CTInterval;
use \App\APIExceptions;
use \App\SeatUtil;
use \App\Summary;
use Response;
use Illuminate\Support\Facades\Input;
use SimpleXMLElement;
ini_set('memory_limit', '-1');
class APIController extends Controller {


    public function __construct()
    {
    }

    public function MUIntervalAPICall(Request $dte)
    {
      $date = $dte->dte;
      if(isset($_GET['end_dte'])){
        $end_date = Input::get('end_dte');
        $operator = '>=';
      }
      else {
        $operator = '=';
      }
      $element_language = $dte->language;
      $element_customer = $dte->customer;
      $element_contract = $dte->contract;
      $element_subcontract = $dte->subcontract;
      $element = $dte->element;
      $mu_interval= MUInterval::select()
        ->where('dte', $operator, $date);
        if(isset($_GET['end_dte'])){
        $mu_interval = $mu_interval->where('dte', '<=', $end_date);
        }
        if(isset($_GET['customer'])){
        $mu_interval = $mu_interval->where('element_customer', $element_customer);
        }
        if(isset($_GET['contract'])){
        $mu_interval = $mu_interval->where('element_contract', $element_contract);
        }
        if(isset($_GET['subcontract'])){
        $mu_interval = $mu_interval->where('element_subcontract', $element_subcontract);
        }
        if(isset($_GET['language'])){
        $mu_interval = $mu_interval->where('element_language', $element_language);
        }
        if(isset($_GET['element'])){
        $mu_interval = $mu_interval->where('element', $element);
        }
        $mu_interval = $mu_interval->get()->toArray();
      function array_to_xml( $data, &$xml_data ) {
          foreach( $data as $key => $value ) {
              if( is_array($value) ) {
                  $key = 'Exception';
                  $subnode = $xml_data->addChild($key);
                  array_to_xml($value, $subnode);
              } else {
                  $xml_data->addChild("$key",htmlspecialchars("$value"));
              }
           }
      }
      $xml_data = new SimpleXMLElement('<?xml version="1.0"?><muExceptions></muExceptions>');
      array_to_xml($mu_interval,$xml_data);
      $result = $xml_data->asXML();
      return Response::make($result, '200')->header('Content-Type', 'text/xml');
    }


    public function CTIntervalAPICall(Request $dte)
    {
      $date = $dte->dte;
      if(isset($_GET['end_dte'])){
        $end_date = Input::get('end_dte');
        $operator = '>=';
      }
      else {
        $operator = '=';
      }
      $element_language = $dte->language;
      $element_customer = $dte->customer;
      $element_contract = $dte->contract;
      $element_subcontract = $dte->subcontract;
      $element = $dte->element;
      $ct_interval= CTInterval::select()
        ->where('dte', $operator, $date);
        if(isset($_GET['end_dte'])){
        $ct_interval = $ct_interval->where('dte', '<=', $end_date);
        }
        if(isset($_GET['customer'])){
        $ct_interval = $ct_interval->where('element_customer', $element_customer);
        }
        if(isset($_GET['contract'])){
        $ct_interval = $ct_interval->where('element_contract', $element_contract);
        }
        if(isset($_GET['subcontract'])){
        $ct_interval = $ct_interval->where('element_subcontract', $element_subcontract);
        }
        if(isset($_GET['language'])){
        $ct_interval = $ct_interval->where('element_language', $element_language);
        }
        if(isset($_GET['element'])){
        $ct_interval = $ct_interval->where('element', $element);
        }
        $ct_interval = $ct_interval->get()->toArray();
      function array_to_xml( $data, &$xml_data ) {
          foreach( $data as $key => $value ) {
              if( is_array($value) ) {
                  $key = 'Exception';
                  $subnode = $xml_data->addChild($key);
                  array_to_xml($value, $subnode);
              } else {
                  $xml_data->addChild("$key",htmlspecialchars("$value"));
              }
           }
      }
      $xml_data = new SimpleXMLElement('<?xml version="1.0"?><ctForecast></ctForecast>');
      array_to_xml($ct_interval,$xml_data);
      $result = $xml_data->asXML();
      return Response::make($result, '200')->header('Content-Type', 'text/xml');
    }


    public function SeatUtilizationAPICall(Request $dte)
    {
      $date = $dte->dte;
      if(isset($_GET['end_dte'])){
        $end_date = Input::get('end_dte');
        $operator = '>=';
      }
      else {
        $operator = '=';
      }
      $element_language = $dte->language;
      $element_customer = $dte->customer;
      $element_contract = $dte->contract;
      $element_subcontract = $dte->subcontract;
      $element = $dte->element;
      $seat_util= SeatUtil::select()
        ->where('dte', $operator, $date);
        if(isset($_GET['end_dte'])){
        $seat_util = $seat_util->where('dte', '<=', $end_date);
        }
        if(isset($_GET['customer'])){
        $seat_util = $seat_util->where('element_customer', $element_customer);
        }
        if(isset($_GET['contract'])){
        $seat_util = $seat_util->where('element_contract', $element_contract);
        }
        if(isset($_GET['subcontract'])){
        $seat_util = $seat_util->where('element_subcontract', $element_subcontract);
        }
        if(isset($_GET['language'])){
        $seat_util = $seat_util->where('element_language', $element_language);
        }
        if(isset($_GET['element'])){
        $seat_util = $seat_util->where('element', $element);
        }
        $seat_util = $seat_util->get()->toArray();
      function array_to_xml( $data, &$xml_data ) {
          foreach( $data as $key => $value ) {
              if( is_array($value) ) {
                  $key = 'Exception';
                  $subnode = $xml_data->addChild($key);
                  array_to_xml($value, $subnode);
              } else {
                  $xml_data->addChild("$key",htmlspecialchars("$value"));
              }
           }
      }
      $xml_data = new SimpleXMLElement('<?xml version="1.0"?><SeatUtilization></SeatUtilization>');
      array_to_xml($seat_util,$xml_data);
      $result = $xml_data->asXML();
      return Response::make($result, '200')->header('Content-Type', 'text/xml');
    }



    
    public function ElementSummaryAPICall(Request $customer)
    {
      $element_customer = $customer->customer;
      $element_language = $customer->language;
      $element_site = $customer->site;
      $element_contract = $customer->contract;
      $element_subcontract = $customer->subcontract;
      $summary= Summary::select();
      
      if(isset($_GET['customer'])){
        $summary = $summary->where('element_customer', $element_customer);
        }
      if(isset($_GET['contract'])){
        $summary = $summary->where('element_contract', $element_contract);
        }
      if(isset($_GET['subcontract'])){
        $summary = $summary->where('element_subcontract', $element_subcontract);
        }
      if(isset($_GET['language'])){
        $summary = $summary->where('element_language', $element_language);
        }
      if(isset($_GET['site'])){
        $summary = $summary->where('element_site', $element_site);
        }

      $summary = $summary->get()->toArray();

      function array_to_xml( $data, &$xml_data ) {
          foreach( $data as $key => $value ) {
              if( is_array($value) ) {
                  $key = 'Exception';
                  $subnode = $xml_data->addChild($key);
                  array_to_xml($value, $subnode);
              } else {
                  $xml_data->addChild("$key",htmlspecialchars("$value"));
              }
           }
      }
      $xml_data = new SimpleXMLElement('<?xml version="1.0"?><summary></summary>');
      array_to_xml($summary,$xml_data);
      $result = $xml_data->asXML();
      return Response::make($result, '200')->header('Content-Type', 'text/xml');
    }

    public function ExceptionsAPICall()
    {

      $exceptions= APIExceptions::select()->get()->toArray();
      
      function array_to_xml( $data, &$xml_data ) {
          foreach( $data as $key => $value ) {
              if( is_array($value) ) {
                  $key = 'Exception';
                  $subnode = $xml_data->addChild($key);
                  array_to_xml($value, $subnode);
              } else {
                  $xml_data->addChild("$key",htmlspecialchars("$value"));
              }
           }
      }
      $xml_data = new SimpleXMLElement('<?xml version="1.0"?><ActivityCodes></ActivityCodes>');
      array_to_xml($exceptions,$xml_data);
      $result = $xml_data->asXML();
      return Response::make($result, '200')->header('Content-Type', 'text/xml');
    }

}