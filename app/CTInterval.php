<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CTInterval extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'app_wfm_api.tbl_ct';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = null;
  public $timestamps = false;

}
